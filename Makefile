ROOT_DIR := $(abspath $(lastword $(MAKEFILE_LIST)))
DEFAULT_GOAL := build

default: test

.PHONY: help
help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  test    		to run tests using pytest"
	@echo "  build    		to run the tests and then build the project"
	@echo "  docker-build	to run the tests and then build the docker project image"
	@echo "  docker-test    to run tests using pytest inside the docker project image"

.PHONY: test
test:
	python3.6 setup.py test

.PHONY: build
build: test
	python3.6 setup.py build

.PHONY: install
install:
	python3.6 setup.py install

.PHONY: docker-build
docker-build: test
	docker build -t songs .

.PHONY: docker-test
docker-test:
	docker-compose run --rm songs python3.6 setup.py test
