# Songs Rating Project

## Code Architecture

All the code architecture focus in making the code life cycle easier by splitting components to ensure each has it owns responsibility and business rules strictly declared. This also helps the development of tests focused on unit testing.

Worthy to mention the project API was built aiming in don't rely in any library or framework:

- **API** doesn't use **Flask directly**.
- **Database** doesn't use **MongoDB directly**.


### Package namespacing

The current project namespace have this composition:

The modules:


| Package                    | Description                                     |
|----------------------------|-------------------------------------------------|
| **songs.api**              | Songs API Flask's Blueprint declaration.        |
| **songs.api.endpoint**     | The abstract Endpoint implementation            |
| **songs.api.endpoints**    | Declaration of each endpoint                    |
| **songs.apps**             | Declares app services                           |
| **songs.cache**            | In memory cache implementation                  |
| **songs.cli**              | Command line tools                              |
| **songs.cli.load\_json**   | Loads Songs from a JSON file                    |
| **songs.cli.runserver**    | Executes a http server with the api endpoints   |
| **songs.db**               | Implements the database service                 |
| **songs.service.database** | Abstracts the business rule and database access |
| **songs.settings**         | Load settings vars from the environment         |


### Database Layer

Available at **songs/service/database.py** the class **SongsServiceDatabase** it's a *bridge* class responsible to abstract database rules made by the API.

It wraps the database calls:

1. Normalizing the return values (and exceptions)
2. Logging stuff
3. Doing minor parameters validation.


The normalizing and validation is made by the **Song** class that abstracts the
data holded by a song. This should be used by *any* Database implementation.

The database layer implementation is available at  **SongsDB** at **songs/db.py**, making sure the calls are properly encapsulated to raise the properly exceptions if an error occurs. This implementation persists the data on **MongoDB**.


#### Service Classes

Some classes used by the Database Layer Service.

| Class       | Description                                                        |
|-------------|--------------------------------------------------------------------|
| songs.service.database.RateValueError | Raised when a wrong Rating value is used |
| songs.service.database.SongNotFound   | Raised when the Song isn't available     |
| songs.service.database.Song           | Abstracts Song data                      |


## Setup

### Requirements

This project runs over python **3.6** version.

The requirements for production environment are available at file **requirements/production.txt** and for development environment at **requirements/development.txt** as well.


### Configuration

Each configuration value is read from the environment variables. That are:

| Var             | Default                    |
|-----------------|----------------------------|
| MONGODB_URI     | mongodb://localhost:27017/ |
| RUNSERVER_HOST  | 0.0.0.0                    |
| RUNSERVER_PORT  | 8000                       |
| RUNSERVER_DEBUG | true                       |
| TEST_MODE       | true                       |



## Development

Most of the development operations are abstract on the *Makefile* and to ensure
the environment for each developer is the same there is a *docker-compose*
declaration available.

### Makefile

There is a Makefile with the basic commands.

    Please use `make <target>' where <target> is one of
      test        to run tests using pytest
      build       to run the tests and then build the project
      docker-build  to run the tests and then build the docker project image
      docker-test    to run tests using pytest inside the docker project image


### Docker image and services

The project has support to docker, the services are declared over the file **docker-services.yml**.

You can the command below to build the image:

    $ make docker-build


### Testing

The tests should be written at the **tests/** folder, you can use *unittest* or *pytest* style.

You can execute the project tests available on **tests/** folder by invoking Makefile command *test*:

    $ make test

This command invokes **pytest** to discover tests around the project working folder, execute them and show the code coverage.

**ATTENTION**: Make sure the dependencies are installed or you can invoke this
command from the docker service *songs* via:

    $ docker-compose run --rm make test


### Before push

Make sure the requirements are valid:

1. Write the Changelog.md
2. Coverage above 95%.
3. Flake8 linter don't show any warnings.
4. All tests are passing.
5. Docker image build is sucesful.


## Running

There is a **runserver** command that invokes a basic http server for development
purposes. After the package is installed it can be invoked via:

    $ songs-runserver

### Importing Data

There is a command available to import data called **songs-load-json**:

    usage: /usr/local/bin/songs-load-json [-h] songs_json_filepath
    positional arguments:
      songs_json_filepath  Path to Songs JSON file.
    optional arguments:
      -h, --help           show this help message and exit

Before importing the JSON data the structured is validated by *jsonschema definitions*.


## TODO

This project still needs some nice features, small and major changes.

### Database Improvements

- **Songs statistics** — After rating a song, these statistics should be built
again and stored over the Song document.
