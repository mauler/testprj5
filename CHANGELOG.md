# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- README for release.
- Cache layer


## [0.0.2] - 2018-05-17

### Added
- Added endpoints for songs retrieve, search, average, statistics and rating.
- Added endpoint handler for exceptions.
- Added logging to the Database layer

### Changed
- Removed MONGODB_COLLECTION from settings
- Removed settings from database layer
- Moved endpoint abstraction to songs.api.endpoint


## [0.0.1] - 2018-05-11

### Added
- Added Songs Service Database layer.
- Added packaging support.
- Added docker image.
- Added tests coverage structure.
- Added runserver and load_json command.
