import argparse
import sys

from json import load
from os.path import exists
from pprint import pformat

from jsonschema import validate, ValidationError

from songs import apps


class LoadJsonCommand:
    JSON_SCHEMA = {
        'type': 'array',
        'items': {
            'type': 'object',
            'required': ['artist', 'title', 'difficulty', 'level', 'released'],
            'properties': {
                'artist': {'type': 'string'},
                'title': {'type': 'string'},
                'difficulty': {'type': 'number'},
                'level': {'type': 'number'},
                'released': {
                    'type': 'string',
                    # NOTE: This validation isn't optimal
                    'pattern': r'\d{4}-\d{2}-\d{2}'
                },
            }
        }
    }

    def __init__(self, args=sys.argv[0]):
        """
        Instances a new commnad line handler for load_json command.

        :param args: Commmand line arguments.
        :type args: list
        """
        self.parser = parser = argparse.ArgumentParser(
            args,
            epilog='The JSON structure should be follow this '
            'JSON Schema:\n{}'.format(pformat(self.JSON_SCHEMA)))

        parser.add_argument('songs_json_filepath',
                            type=str,
                            help='Path to Songs JSON file.',
                            default='/dev/stdin')

    @classmethod
    def _validate_songs_json_data(cls, songs_data):
        """Validates if the songs data structure is fits the schema proposed.

        :param songs_data: Data structure
        :type songs_data: list

        :raises: SchemaErrror
        """
        try:
            validate(songs_data, cls.JSON_SCHEMA)
        except ValidationError as exc:
            print('Error validating json contents')
            raise exc

    @classmethod
    def _validate_songs_json_arg(cls, songs_json_path):
        """Validates if the songs json file path is valid.

        :param songs_json_path: Path to JSON songs
        :type songs_path: str

        :raises: SystemExit
        """
        if not exists(songs_json_path):
            raise SystemExit(
                1,
                'JSON file "{}" does not exists'.format(songs_json_path))

    @classmethod
    def _load_songs_json(cls, songs_json_path):
        """Loads JSON format data to Python object.

        :param songs_json_path: Path to JSON file
        :type songs_json_path: str

        :returns: Parsed json data
        :rtype: list
        """
        with open(songs_json_path) as f:
            return load(f)

    @classmethod
    def _run(cls, songs_data):
        """Executes the command operation, loads data into database

        :param songs_data: The data to be loaded
        :type songs_data: list
        """
        print('Loading songs into database')
        songs = 0
        for item in songs_data:
            cls._save_song(item)
            songs += 1

        print('Songs loaded into database: {}'.format(songs))

    @classmethod
    def _save_song(cls, song_data):
        """Saves a song into database.

        :param song_data: Song data
        :type song_data: dict
        """
        song = apps.songsdb.make_song(song_data['title'],
                                      song_data['artist'],
                                      song_data['difficulty'],
                                      song_data['level'],
                                      song_data['released'])
        apps.songsdb.save_song(song)

    def run(self):
        """Runs the command, check the arguments."""
        args = self.parser.parse_args()

        print('Validating songs JSON')
        self._validate_songs_json_arg(args.songs_json_filepath)

        print('Loading songs JSON')
        songs_data = self._load_songs_json(args.songs_json_filepath)

        print('Validating songs JSON data')
        self._validate_songs_json_data(songs_data)

        self._run(songs_data)


def main():
    LoadJsonCommand().run()


if __name__ == '__main__':
    main()
