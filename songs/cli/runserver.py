#!flask/bin/python

from songs import apps
from songs import settings


def main():
    app = apps.make_flask_app()
    app.run(host=settings.RUNSERVER_HOST,
            port=settings.RUNSERVER_PORT,
            debug=settings.RUNSERVER_DEBUG)


if __name__ == '__main__':
    main()
