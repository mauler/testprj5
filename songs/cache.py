import logging
from datetime import datetime, timedelta
# from functools import update_wrapper
from json import dumps


LOGGER = logging.getLogger(__name__)


class CacheCall:
    """Abstracts the cached call. """

    def __init__(self, cache, fn, timeout):
        """Configures a caching call.

        :param cache: The cache layer.
        :type cache: Cache

        :param fn: call to be cached
        :type fn: callable

        :param timeout: Cache timeout
        :type timeout: int
        """
        # update_wrapper(self, fn)
        self._cache = cache
        self._fn = fn
        self._timeout = timeout

    def __call__(self, *args, **kwargs):
        """Executes the call if not key available at the cache layer.

        :returns: The decorated call results from cache layer or the fresh call
        """
        # print('SELF_ARGS', self)
        # print('ARGS', args)
        # print('KWARGS', kwargs)
        cachekey = self._cache.make_key(self._fn.__name__, *args, **kwargs)
        expired = self._cache.get_key_info(cachekey)
        if expired is None:
            # print('FN_ARGS', self._fn, args, kwargs)
            value = self._fn(*args, **kwargs)
            LOGGER.debug('Setting cache value: {}'.format(cachekey))
            self._cache.set_key(cachekey, value, self._timeout)
            return value
        else:
            LOGGER.debug('Retrieving cache value: {}'.format(cachekey))
            return self._cache.get_key_value(cachekey)


class Cache:
    """Helper class to abstract caching based on key -> value. """

    def __init__(self, default_timeout=30, enable_cache=True):
        """Configures the cache layer.

        :param default_timeout: The default timeout for every cached call.
        :type default_timeout: int

        :param enable_cache: Enable caching engine
        :type enable_cache: bool
        """
        self._default_timeout = default_timeout
        self._data = {}
        self._data_timeout = {}
        self._enabled = enable_cache

    def cache(self, fn_or_timeout, *args, **kwargs):
        # When timeout isn't set, just return the CacheCall
        if callable(fn_or_timeout):
            if not self._enabled:
                return fn_or_timeout

            fn = fn_or_timeout
            timeout = self._default_timeout
            return CacheCall(cache=self, fn=fn, timeout=timeout)

        # When timeout is set return a function that will decorate the call
        else:
            def decorate(fn, *args, **kwargs):
                if not self._enabled:
                    return fn

                timeout = fn_or_timeout
                return CacheCall(cache=self, fn=fn, timeout=timeout)

            return decorate

    def get_key_value(self, key):
        """Returns cache key value.

        :param key: the key
        :type key: str

        :returns: anything
        :rtype: any
        """
        return self._data[key]

    def get_key_info(self, key):
        """Returns keys expiration time in seconds or None if already expired.

        :param key: the key
        :type key: str

        :returns: timeout in seconds
        :rtype: int or None
        """
        if key not in self._data:
            return None
        else:
            return (self._data_timeout[key] - datetime.now()).seconds

    def make_key(self, *args, **kwargs):
        """Make a serialized key based on a function call.

        :param args: tuple with function call args
        :type args: tuple

        :param kwargs: dict with function call args
        :type kwargs: dict

        :returns: key serialized string
        :rtype: str
        """
        return '{}:{}'.format(dumps(args), dumps(kwargs, sort_keys=True))

    def set_key(self, key, value, timeout):
        """Sets key value.

        :param key: cache key
        :type key: str

        :param value: the value to be saved.
        :type value: any

        :returns: expiration datetime
        :rtype: datetime.datetime
        """
        self._data[key] = value
        self._data_timeout[key] = datetime.now() + timedelta(seconds=timeout)
        return self._data_timeout[key]
