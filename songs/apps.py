from flask import Flask

from songs.db import SongsDB
from songs import settings


# Configures the Database layer
songsdb = SongsDB(settings.MONGODB_URI, settings.MONGODB_DATABASE)


# Makes the Flask App
def make_flask_app():

    from songs.api import songs_api
    from songs.api.helpers import APIJSONEncoder

    flaskapp = Flask(__name__)

    flaskapp.json_encoder = APIJSONEncoder

    flaskapp.register_blueprint(songs_api)

    return flaskapp
