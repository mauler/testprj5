import decimal

from logging import getLogger
from datetime import datetime
from decimal import Decimal

from bson.decimal128 import Decimal128, create_decimal128_context
from bson.objectid import ObjectId
from pymongo import MongoClient

from songs.service.database import (SongsServiceDatabase, Song,
                                    RateValueError, SongNotFound)


LOGGER = getLogger(__name__)


class SongsDB(SongsServiceDatabase):
    """Bridge class to abstract the calls to Mongo collections."""
    PAGE_SIZE = 10

    def __init__(self, mongodb_uri, mongodb_database,
                 songs_collection='SONGS',
                 rating_collection='RATING'):
        """Instances a new Songs Service Database Layer.

        :param mongodb_uri: URI to MongoDB instances
        :type mongodb_uri: str

        :param mongodb_database: MongoDB database
        :type mongodb_database: str

        :param songs_collection: MongoDB collection for songs
        :type songs_collection: str

        :param rating_collection: MongoDB collection for songs rating
        :type rating_collection: str
        """
        self._client = MongoClient(mongodb_uri)
        self._db = self._client[mongodb_database]
        self._songs = self._db[songs_collection]
        self._rating = self._db[rating_collection]

    @classmethod
    def _document2song(cls, document):
        """Returns a Song model instance from a MongoDB document.

        :para document: MongoDB document
        :type document: dict

        :returns: Song model instance
        :rtype: Song
        """
        return Song(document['title'],
                    document['artist'],
                    document['difficulty'].to_decimal(),
                    document['level'],
                    document['released'],
                    str(document['_id']))

    @classmethod
    def _validate_rating_value(cls, rate):
        if rate not in [1, 2, 3, 4, 5]:
            raise RateValueError('Invalid rating value')

    @classmethod
    def _build_find_params(cls, search=None, level=None):
        """Build the mongo query params to be passed to find()

        :param search: Words to be searched on Song title and Artist name
        :type search: str

        :level: The song level
        :type level: int

        :returns: Mongo query find params
        :rtype: dict
        """

        params = {}
        if search:
            ors = []
            # Split the words
            words = search.strip().split(' ')
            for word in words:
                ors.append({'title': {'$regex': word, '$options': 'i'}})
                ors.append({'artist': {'$regex': word, '$options': 'i'}})
            params['$or'] = ors

        if level:
            params['level'] = level

        return params or None

    def _build_query(self, search=None, level=None, page=1):
        """Build the mongo query to be evaluated.

        :param search: Words to be searched on Song title and Artist name
        :type search: str

        :level: The song level
        :type level: int

        :returns: Mongo query find params
        :rtype: dict
        """

        params = self._build_find_params(search, level)

        skip = (page - 1) * self.PAGE_SIZE

        qry = self._songs.find(params).skip(skip).limit(self.PAGE_SIZE)

        return qry

    def _find_song(self, song_id):
        """Returns the Song document for the desired ID.

        :param song_id: Song ID
        :type song_id: int

        :returns: Song document if available
        :rtype: dict, None
        """
        return self._songs.find_one({'_id': ObjectId(song_id)})

    def _get_song(self, song_id):
        """Returns the Song model instance for the desired ID.

        :param song_id: Song ID
        :type song_id: int

        :raises: SongNotFound

        :returns: Song instance
        :rtype: Song
        """
        doc = self._find_song(song_id)
        if doc is None:
            raise SongNotFound('Song ID {} not found'.format(song_id))

        return self._document2song(doc)

    def search_songs(self, search, page=1):
        """Search for songs into database, check for the text against song
        title and artist name.

        :param search: The search phrase
        :type search: str

        :param page: Result page
        :type page: int

        :returns: Iterator with instances of Song model
        :rtype: list_iterator
        """

        LOGGER.debug('Search for songs containing: {}'.format(search))

        for document in self._build_query(search=search, page=page):
            yield self._document2song(document)

    def get_songs(self, page=1):
        """Retrieves songs from the database.

        :param page: Result page
        :type page: int

        :returns: Iterator with instances of Song model
        :rtype: list_iterator
        """

        LOGGER.debug('Retrieving songs for page: {}'.format(page))

        for document in self._build_query(page=page):
            yield self._document2song(document)

    def save_song(self, song):
        """Saves a song object into Mongo database.

        :param song: Song instance to saved to database.
        :type song: Song

        :returns: Song Unique ID
        :rtype: str
        """

        # Converts Decimal to Decimal128
        with decimal.localcontext(create_decimal128_context()) as ctx:
            difficulty = Decimal128(ctx.create_decimal(song.difficulty))

        released = datetime(song.released.year,
                            song.released.month,
                            song.released.day)

        doc = {
            'title': song.title,
            'artist': song.artist,
            'difficulty': difficulty,
            'level': song.level,
            'released': released,
        }

        result = self._songs.insert_one(doc)

        LOGGER.info('Saved new song at database: {}'.format(song.title))

        song_id = str(result.inserted_id)

        return song_id

    def get_average_difficulty(self):
        """Returns the average difficulty for a certain level.

        :returns: The average
        :rtype: decimal.Decimal
        """
        pipeline = [{
            '$group': {
                '_id': None,
                'difficulty_avg': {'$avg': '$difficulty'},
            }
        }]

        LOGGER.debug('Retrieving difficulty average')

        for result in self._songs.aggregate(pipeline):
            return result['difficulty_avg'].to_decimal()

    def get_average_difficulty_by_level(self, level):
        """Returns the average difficulty for the songs selected level.

        :param level: The song level
        :type level: int

        :returns: The average
        :rtype: decimal.Decimal
        """
        pipeline = [
            {
                '$match': {
                    'level': level,
                }
            },
            {
                '$group': {
                    '_id': None,
                    'difficulty_avg': {'$avg': '$difficulty'},
                }
            }
        ]

        LOGGER.debug('Retrieving difficulty average for level: {}'
                     .format(level))

        for result in self._songs.aggregate(pipeline):
            return result['difficulty_avg'].to_decimal()

    def rate_song(self, song, rate):
        """Adds a new rating for the song, the rate is saved on another
        collection.

        :param song: Song instance to saved to database.
        :type song: Song

        :param rate: The rating, between 1-5
        :type rate: int

        :raises: RateValueError, SongNotFound
        """

        self._find_song(song.song_id)

        self._validate_rating_value(rate)

        return self._rating.insert_one({
            'song_id': song.song_id,
            'rate': rate,
        })

    def get_song_statistics(self, song):
        """Returns rating statistics for a Song instance, the static are
        retrieved from the rating collection.

        :param song: Song instance to saved to database.
        :type song: Song

        :returns: Song rating statistics
        :rtype: dict

        :raises: SongNotFound
        """

        # Ensure song does exists
        self._get_song(song.song_id)

        pipeline = [
            {
                '$match': {
                    'song_id': song.song_id,
                }
            },
            {
                '$group': {
                    '_id': None,
                    'average': {'$avg': '$rate'},
                    'lowest': {'$min': '$rate'},
                    'highest': {'$max': '$rate'},
                }
            }
        ]

        LOGGER.debug('Retrieving statistics for Song: {}'
                     .format(song.song_id))

        for result in self._rating.aggregate(pipeline):
            return {
                'average': Decimal(result['average']),
                'lowest': result['lowest'],
                'highest': result['highest'],
            }
