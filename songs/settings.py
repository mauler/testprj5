from os import environ

MONGODB_URI = environ.get('MONGODB_URI', 'mongodb://localhost:27017/')

MONGODB_DATABASE = environ.get('MONGODB_DATABASE', 'songs-application')

RUNSERVER_HOST = environ.get('RUNSERVER_HOST', '0.0.0.0')

RUNSERVER_PORT = int(environ.get('RUNSERVER_PORT', 8000))

RUNSERVER_DEBUG = environ.get('RUNSERVER_DEBUG', 'true').lower() == 'true'

TEST_MODE = environ.get('TEST_MODE', 'true').lower() == 'true'
