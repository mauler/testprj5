import abc

from datetime import date, datetime
from decimal import Decimal


class RateValueError(ValueError):
    """Exception to be raised when the Rate value is invalid. """


class SongNotFound(ValueError):
    """Exception to be raised when a specific Song ID isn't valid. """


class Song:
    """Model used to hold song data across the application. """

    def __init__(self, title, artist, difficulty, level, released,
                 song_id=None):
        """ Instances new song object.

        :param title: The song title
        :type title: str

        :param artist: The song artist's name
        :type artist: str

        :param difficulty: The song difficulty level of play
        :type difficulty: Decimal

        :param level: The song level
        :type level: int

        :param released: The song release date
        :type released: datetime.date

        :param song_id: Optional Song ID
        :type song_id: str
        """

        self._validate(title, artist, difficulty, level, released)
        self.title = title
        self.artist = artist
        self.difficulty = difficulty
        self.level = level
        self.released = released
        self.song_id = song_id

    def _validate(self, title, artist, difficulty, level, released):
        """ Validates attributes to instance the model properly.

        :param title: The song title
        :type title: str

        :param artist: The song artist's name
        :type artist: str

        :param difficulty: The song difficulty level of play
        :type difficulty: Decimal

        :param level: The song level
        :type level: int

        :raises: TypeErrro, ValueError
        """
        if not isinstance(title, str):
            raise TypeError('Title should be a string')

        if len(title) < 3:
            raise ValueError('Title should be at least 3 char long')

        if not isinstance(artist, str):
            raise TypeError('Artist should be a string')

        if len(artist) < 3:
            raise ValueError('Artist should be least 3 char long')

        if not isinstance(difficulty, Decimal):
            raise TypeError('Difficulty should be a decimal')

        if difficulty < 0:
            raise ValueError('Difficulty should be positive')

        if not isinstance(level, int):
            raise TypeError('Level should be a integer')

        if level < 0:
            raise ValueError('Level should be positive')

        if not isinstance(released, date):
            raise TypeError('Released should be a valid date object')


class SongsServiceDatabase(metaclass=abc.ABCMeta):

    def make_song(self, title, artist, difficulty, level, released):
        """Instances a Song object, ensure all parameters are converted.

        :param title: The song title
        :type title: str

        :param artist: The song artist's name
        :type artist: str

        :param difficulty: The song difficulty level of play
        :type difficulty: Decimal

        :param level: The song level
        :type level: int

        :param released: The song release date: YYYY-MM-DD
        :type released: str

        :returns: A new Song object
        :rtype: Song

        :raises: TypeError
        """

        title = str(title)
        artist = str(artist)
        difficulty = Decimal(difficulty)
        level = int(level)
        released = datetime.strptime(released, '%Y-%m-%d').date()

        return Song(title, artist, difficulty, level, released)

    @abc.abstractmethod
    def get_songs(self, page=1):
        """Returns Song instances for a specific page.

        :param page: Result page
        :type page: int

        :returns: List of Songs
        :rtype: list_iterator
        """

    @abc.abstractmethod
    def save_song(self, song):
        """ This method should be implemented on your database backend.

        :param song: Song instance to saved to database.
        :type song: Song

        :returns: Song Unique ID
        :rtype: str
        """

    @abc.abstractmethod
    def get_average_difficulty(self):
        """Should return the average difficulty for the songs available
        in the database.

        :returns: The average
        :rtype: decimal.Decimal
        """

    @abc.abstractmethod
    def get_average_difficulty_by_level(self, level):
        """Should return the average difficulty for the songs of a certain
        level available in the database.

        :param level: The song leve
        :type level: int

        :returns: The average
        :rtype: decimal.Decimal
        """

    @abc.abstractmethod
    def search_songs(self, message):
        """Should search for songs containing the message in the song title
        or artist name.

        :param message: The search phrase
        :type level: str

        :returns: List of Songs
        :rtype: list_iterator
        """

    @abc.abstractmethod
    def rate_song(self, song, rating):
        """Rate a song, the rating shuld be between 1-5.

        :param song: Song instance to saved to database.
        :type song: Song

        :param rating: The rating, between 1-5
        :type rating: int

        :raises: RateValueError, SongNotFound
        """

    @abc.abstractmethod
    def get_song_statistics(self, song):
        """Returns rating statistics for a song.

        :param song: Song instance to saved to database.
        :type song: Song

        :returns: Song rating statistics
        :rtype: dict

        :raises: SongNotFound
        """
