from songs.api.endpoint import Endpoint, EndpointBadRequest
from songs.cache import Cache
from songs import settings


CACHE = Cache(enable_cache=not settings.TEST_MODE)


class ListSongs(Endpoint):
    PAGE_ARG = 'page'

    methods = ['GET']

    @classmethod
    def _get_page(cls, args):
        """Validates and normalize page number from the request's querystring
        argument.

        :param args: Querystring arguments
        :dict args: dict

        :raises: ValueError

        :returns: Normalized page number
        :rtype: int
        """

        if cls.PAGE_ARG in args:
            page = args[cls.PAGE_ARG]
            exc = EndpointBadRequest('Invalid page number: {}'
                                     .format(page))

            try:
                page = int(page)
                if page < 1:
                    raise exc

            except ValueError:
                raise exc

            return page

        else:
            return 1

    @CACHE.cache(60 * 5)
    def view(self, method, args, form):
        """List songs paginated. """
        page = self._get_page(args)
        return self.db.get_songs(page)


class GetSongsAverageDifficulty(Endpoint):
    LEVEL_ARG = 'level'

    methods = ['GET']

    @classmethod
    def _get_level(cls, args):
        if cls.LEVEL_ARG in args:
            level = args[cls.LEVEL_ARG]
            exc = EndpointBadRequest('Invalid level value: {}'.format(level))

            try:
                level = int(level)

                if level < 1:
                    raise exc

            except ValueError:
                raise exc

            return level

    # Cache for 7 days (No specific reason)
    @CACHE.cache(60 * 60 * 24 * 7)
    def view(self, method, args, form):
        level = self._get_level(args)

        if level is None:
            return self.db.get_average_difficulty()
        else:
            return self.db.get_average_difficulty_by_level(level)


class SearchSongs(Endpoint):
    MESSAGE_ARG = 'message'

    methods = ['GET']

    @classmethod
    def _get_message(cls, args):
        message = args.get(cls.MESSAGE_ARG)
        exc = EndpointBadRequest('Query argument "message" should have a '
                                 'valid value')
        if not message:
            raise exc

        if len(message) < 5:
            raise exc

        return message

    @CACHE.cache(60)
    def view(self, method, args, form):
        message = self._get_message(args)
        return self.db.search_songs(message)


class RateSong(Endpoint):
    SONG_ID_ARG = 'song_id'
    RATING_ARG = 'rating'

    methods = ['POST']

    def view(self, method, args, form):
        song = self.db._get_song(form.get(self.SONG_ID_ARG))
        self.db.rate_song(song, form.get(self.RATING_ARG))
        return {'description': 'success'}


class GetSongStatistics(Endpoint):
    methods = ['GET']

    # Cache for 1 day
    @CACHE.cache(60 * 60 * 24 * 1)
    def view(self, method, args, form, song_id):
        return self.db.get_song_statistics(song_id)
