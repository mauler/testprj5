import inspect
import json

from decimal import Decimal


from songs.service.database import Song


class APIJSONEncoder(json.JSONEncoder):
    """ Encode Song instances and other special types such as generator and
    Decimal."""

    @classmethod
    def _get_song_dict(cls, song):
        """ Transforms an Song object into a dict.

        :param song: The Song instance
        :type song: Song

        :returns: dict instance for the Song instance
        :rtype: dict
        """
        return {
            'title': song.title,
            'artist': song.artist,
            'difficulty': song.difficulty,
            'level': song.level,
            'released': song.released.strftime('%Y-%m-%d'),
            'song_id': song.song_id,
        }

    def default(self, obj):
        """ Ensures all objects are json type compatible.
        Converts Song instances, date and Decimal. """
        if inspect.isgenerator(obj):
            return list(obj)

        # Song model
        if isinstance(obj, Song):
            return self._get_song_dict(obj)

        # Decimal object
        if isinstance(obj, Decimal):
            return float(obj)

        return super(APIJSONEncoder, self).default(obj)
