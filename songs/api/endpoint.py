from abc import ABCMeta, abstractmethod
from functools import partial
from http import HTTPStatus
from json import dumps
from logging import getLogger

from flask import request, jsonify

from songs.api.helpers import APIJSONEncoder
from songs.service.database import SongNotFound


LOGGER = getLogger(__name__)


class EndpointBadRequest(ValueError):
    """Holds EndpointBadrequest errors """


class Endpoint(metaclass=ABCMeta):
    """Abstracts the Endpoint to be processed as a flask view. """

    methods = NotImplemented

    def __init__(self, app, db):
        """Creates a new Endpoint.

        :param app: The Flask application
        :type app: flask.Flask

        :param db: Database layer
        :type db: songs.db.SongsDB
        """
        self.app = app
        self.db = db

    @abstractmethod
    def view(self, method, args, form):
        """ Process the request.

        :param method: The request method
        :type method: str

        :param args: Request parameters from querystring
        :type args: dict

        :param form: Form data from POST and PUT requests
        :type form: dict

        :returns: Response
        :rtype: flask.Response
        """

    @classmethod
    def register(cls, flask_app, db, route):
        """Register an Endoint as a flask view to specific route.

        :param flask_app: The Flask Application
        :type flask_app: flask.Flask

        :param db: The songs database layer
        :type db: songs.db.SongsDB

        :param route: URL as Regular expression
        :type route: str
        """
        endpoint = cls(flask_app, db)
        adapter = EndpointAdapter(flask_app, endpoint)
        call = partial(adapter, request=request)

        # To ensure it's compatible with flask view
        call.__name__ = 'Endpoint{}'.format(endpoint.__class__.__name__)

        flask_app.add_url_rule(route, view_func=call)

        LOGGER.debug('Registered endpoint {} on flask app.'
                     .format(endpoint.__class__.__name__))


class EndpointAdapter:
    """Adapts a Endpoint as Flask view. """

    def __init__(self, app, endpoint):
        self.app = app
        self.endpoint = endpoint

    def _make_error_response(self, exception, status_code):
        """Encapsulates an Exception as a Response object.

        :param exception: The raised Exception
        :type exception: Exception

        :param status_code: HTTP status code
        :type status_code: int

        :returns: Encapsulated Exception as a Flask Response object.
        :rtype: flask.Response
        """
        data = {'description': str(exception)}
        LOGGER.error('HTTP {} Error Response: {}'.format(status_code, data))
        return self.app.response_class(
            status=status_code,
            mimetype='application/json',
            response=dumps(data, cls=APIJSONEncoder))

    def __call__(self, request, *args, **kwargs):
        """Executes the current endpoint, encapsulate any errors.

        :param request: Flask request object
        :type request: flask.Request

        :returns: Flask response object
        :rtype: flask.Response
        """
        LOGGER.debug('Request {} {} {}: {}'.format(request.method,
                                                   request.path,
                                                   request.args,
                                                   request.form))
        try:
            result = self.endpoint.view(request.method,
                                        request.args,
                                        request.form,
                                        *args,
                                        **kwargs)

        except EndpointBadRequest as exc:
            response = self._make_error_response(exc,
                                                 HTTPStatus
                                                 .BAD_REQUEST.value)

        except SongNotFound as exc:
            response = self._make_error_response(exc,
                                                 HTTPStatus
                                                 .NOT_FOUND.value)

        except Exception as exc:
            response = self._make_error_response(exc,
                                                 HTTPStatus
                                                 .INTERNAL_SERVER_ERROR.value)

        else:
            # Jsonify the current result
            response = jsonify(result)

        finally:
            LOGGER.info('Successfully Request {} {}'
                        .format(request.method, request.path))
            return response
