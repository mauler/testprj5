from flask import Blueprint

from songs.api import endpoints

from songs.apps import songsdb

# Flask's Blueprint
songs_api = Blueprint('songs_api', __name__)

# Shortcut for registration
register = lambda endpoint, route: endpoint.register(songs_api, songsdb, route)

register(endpoints.ListSongs, '/songs')

register(endpoints.GetSongsAverageDifficulty, '/songs/avg/difficulty')

register(endpoints.SearchSongs, '/songs/search')

register(endpoints.RateSong, '/songs/rating')

register(endpoints.GetSongStatistics, '/songs/avg/rating/<int:song_id>')
