import setuptools


setuptools.setup(
    setup_requires=[
        'pytest-runner',
    ],

    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-flakes',
        'pytest-pythonpath',
        'pytest-sugar',
    ],

    name='songs',
    version='0.0.1',

    author='Paulo R',
    author_email='proberto.macedo@gmail.com',

    description='Songs rating API.',
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(exclude='tests'),

    entry_points={
        'console_scripts': [
            'songs-load-json = songs.cli.load_json:main',
            'songs-runserver = songs.cli.runserver:main',
        ],
    },

    install_requires=[],

    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6 :: Only',
    ],
)
