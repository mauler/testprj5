FROM python:3-alpine

ENV APP_DIR /usr/src/app

RUN mkdir -pv ${APP_DIR}

WORKDIR ${APP_DIR}

RUN mkdir -pv ${APP_DIR}/requirements

ADD requirements ${APP_DIR}/requirements

RUN pip install -r requirements/production.txt

ADD . ${APP_DIR}

RUN python setup.py install

RUN rm -rfv ${APP_DIR}/* /src/

CMD songs-runserver
