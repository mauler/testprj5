from unittest import TestCase
from unittest.mock import patch, MagicMock

from songs import settings
from songs.cli import runserver


class RunserverTests(TestCase):

    @patch('songs.apps.make_flask_app')
    def test_main(self, make_flask_app_mock):
        make_flask_app_mock.return_value = MagicMock()

        runserver.main()

        make_flask_app_mock.assert_called_with()

        make_flask_app_mock.return_value.run.assert_called_with(
            host=settings.RUNSERVER_HOST,
            port=settings.RUNSERVER_PORT,
            debug=settings.RUNSERVER_DEBUG)
