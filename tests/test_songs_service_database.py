from datetime import date
from decimal import Decimal

import pytest

from songs.service.database import Song


TITLE = 'Song Title'
ARTIST = 'Artist Name'
DIFFICULTY = Decimal(5)
LEVEL = 5
RELEASED = date(2012, 12, 12)


def test_song_model():
    # Basic test
    Song(TITLE, ARTIST, DIFFICULTY, LEVEL, RELEASED)


def test_song_model_validation():
    # Title type
    with pytest.raises(TypeError, message='Title should be a string'):
        Song(None, ARTIST, DIFFICULTY, LEVEL, RELEASED)

    # Title length
    with pytest.raises(ValueError,
                       message='Title should be at least 3 char long'):
        Song('', ARTIST, DIFFICULTY, LEVEL, RELEASED)

    # Artist type
    with pytest.raises(TypeError, message='Artist should be a string'):
        Song(TITLE, None, DIFFICULTY, LEVEL, RELEASED)

    # Artist length
    with pytest.raises(ValueError,
                       message='Artist should be at least 3 char long'):
        Song(TITLE, '', DIFFICULTY, LEVEL, RELEASED)

    # Difficulty type
    with pytest.raises(TypeError, message='Difficulty should be a decimal'):
        Song(TITLE, ARTIST, 'HARD', LEVEL, RELEASED)

    # Difficulty value
    with pytest.raises(ValueError,
                       message='Difficulty should be positive'):
        Song(TITLE, ARTIST, Decimal(-1), LEVEL, RELEASED)

    # Level type
    with pytest.raises(TypeError, message='Level should be a decimal'):
        Song(TITLE, ARTIST, DIFFICULTY, 'low', RELEASED)

    # Level value
    with pytest.raises(ValueError,
                       message='Level should be positive'):
        Song(TITLE, ARTIST, DIFFICULTY, -1, RELEASED)

    # Level value
    with pytest.raises(TypeError,
                       message='Released should be a valid date object'):
        Song(TITLE, ARTIST, DIFFICULTY, LEVEL, 20121212)
