import pytest

from datetime import date
from decimal import Decimal

from songs import settings
from songs.db import SongsDB
from songs.service.database import Song

TEST_MONGODB_DATABASE = '__test_database__'

TITLE = 'Song Title'
ARTIST = 'Artist Name'
DIFFICULTY = Decimal(5)
LEVEL = 60
RELEASED = date(2012, 12, 12)


# Make song titles
TITLE1 = '{} #1 One'.format(TITLE)
TITLE2 = '{} #2 Two'.format(TITLE)
TITLE3 = '{} #3 Three'.format(TITLE)


@pytest.fixture
def songsdb():
    _songsdb = SongsDB(mongodb_uri=settings.MONGODB_URI,
                       mongodb_database=TEST_MONGODB_DATABASE)
    _songsdb._db.command('dropDatabase')
    return _songsdb


@pytest.fixture
def song():
    return Song(TITLE, ARTIST, DIFFICULTY, LEVEL, RELEASED)


def test_get_song_statistics(songsdb, song):
    # Save the song to database
    song_id = songsdb.save_song(song)

    # Sets the Song ID
    song.song_id = song_id

    # Rate songs 3 times: 2, 3 and 4 (min: 2, max: 4, average: 3)
    songsdb.rate_song(song, 2)
    songsdb.rate_song(song, 3)
    songsdb.rate_song(song, 4)

    assert songsdb.get_song_statistics(song) == {'lowest': 2,
                                                 'highest': 4,
                                                 'average': 3}


@pytest.fixture
def songs3(songsdb):

    # Save 3 songs
    songsdb.save_song(Song(TITLE1, ARTIST, DIFFICULTY + 1, LEVEL, RELEASED))

    songsdb.save_song(Song(TITLE2, ARTIST, DIFFICULTY + 2, LEVEL + 10,
                           RELEASED))

    songsdb.save_song(Song(TITLE3, ARTIST, DIFFICULTY + 3, LEVEL, RELEASED))

    songs = list(songsdb.get_songs())

    return songs


def test_get_average_difficulty(songsdb, songs3):
    # Song 1: 6, Song 2: 7, Song 3: 8
    assert songsdb.get_average_difficulty() == 7


def test_get_average_difficulty_by_level(songsdb, songs3):
    # Song 1: 6, Song 2: 7, Song 3: 8
    assert songsdb.get_average_difficulty_by_level(LEVEL) == 7


def test_save_song(songsdb, song):
    # Ensures song was saved into Database
    song_id = songsdb.save_song(song)
    assert songsdb._find_song(song_id)


def test_get_songs(songs3):
    # Check if the songs are returned
    assert len(songs3) == 3

    # NOTE: This test isn't totally optimal
    assert songs3[0].title == TITLE1
    assert songs3[1].title == TITLE2
    assert songs3[2].title == TITLE3


def test_search_songs(songsdb, songs3):
    # Search for the Song #2
    results = songsdb.search_songs(search='Two')
    results = list(results)

    # Ensure 1 result
    assert len(results) == 1

    # Ensure the Song title is the desired title
    assert results[0].title == TITLE2


def test_build_find_params():

    assert SongsDB._build_find_params('Some One') == {
        '$or': [
            {'title': {'$regex': 'Some', '$options': 'i'}},
            {'artist': {'$regex': 'Some', '$options': 'i'}},
            {'title': {'$regex': 'One', '$options': 'i'}},
            {'artist': {'$regex': 'One', '$options': 'i'}},
        ]}

    assert SongsDB._build_find_params(level=1) == {'level': 1}

    assert SongsDB._build_find_params('Some One', level=1) == {
        '$or': [
            {'title': {'$regex': 'Some', '$options': 'i'}},
            {'artist': {'$regex': 'Some', '$options': 'i'}},
            {'title': {'$regex': 'One', '$options': 'i'}},
            {'artist': {'$regex': 'One', '$options': 'i'}},
        ],
        'level': 1,
    }
