from datetime import date
from decimal import Decimal
from http import HTTPStatus
from json import dumps, loads
from unittest import TestCase
from unittest.mock import patch, MagicMock

from songs.apps import make_flask_app
from songs.api.endpoint import EndpointAdapter, EndpointBadRequest
from songs.api.helpers import APIJSONEncoder
from songs.service.database import Song, SongNotFound


APP = make_flask_app()


def app_context(fn):
    """ Decorates a function to use flask app context. """
    def decorated(*args, **kwargs):
        with APP.app_context():
            return fn(*args, **kwargs)

    return decorated


class ApiTests(TestCase):

    SONG_ID = 'abcd1234'
    TITLE = 'Song Title'
    ARTIST = 'Artist Name'
    DIFFICULTY = Decimal(5)
    LEVEL = 5
    RELEASED = date(2012, 12, 12)

    SONG = {
        'title': TITLE,
        'artist': ARTIST,
        'difficulty': float(DIFFICULTY),
        'level': LEVEL,
        'released': RELEASED.strftime('%Y-%m-%d'),
        'song_id': SONG_ID,
    }

    SONG_JSON = dumps(SONG)

    def _api_dump_json(self, obj):
        return dumps(obj, cls=APIJSONEncoder, sort_keys=True).encode()

    def _new_song(self, song_id):
        return Song('{} version {}'.format(self.TITLE, song_id),
                    self.ARTIST,
                    self.DIFFICULTY,
                    self.LEVEL,
                    self.RELEASED,
                    song_id)

    def _song_dict(self, song_id):
        return dict(title='{} version {}'.format(self.TITLE, song_id),
                    artist=self.ARTIST,
                    difficulty=float(self.DIFFICULTY),
                    level=self.LEVEL,
                    released=self.RELEASED.strftime('%Y-%m-%d'),
                    song_id=song_id)

    def setUp(self):
        self.client = APP.test_client()

    @app_context
    def test_apijsonencoder_generator(self):
        """ Tests if the encoder is encoding Song instances and other types p
        properly. """
        self.assertEqual(dumps([0, 1]),
                         dumps((i for i in [0, 1]),
                               cls=APIJSONEncoder))

    @app_context
    def test_apijsonencoder_song(self):
        """ Tests if the encoder is encoding Song instances and other types p
        properly. """
        self.assertEqual(self.SONG_JSON,
                         dumps(Song(self.TITLE,
                                    self.ARTIST,
                                    self.DIFFICULTY,
                                    self.LEVEL,
                                    self.RELEASED,
                                    self.SONG_ID),
                               cls=APIJSONEncoder))

    @app_context
    def test_apijsonencoder_empty_dict(self):
        self.assertEqual(dumps({}),
                         dumps({}, cls=APIJSONEncoder))

    def test_songs(self):

        # Creates a list of Songs to be mocked
        songs = [self._new_song(1), self._new_song(2), self._new_song(3)]

        # Mock the Database layer
        with patch('songs.db.SongsDB.get_songs',
                   return_value=songs) as get_songs_mock:

            response = self.client.get('/songs')

            get_songs_mock.assert_called()

            self.assertEqual(200, response.status_code)

            self.assertEqual([self._song_dict(1),
                              self._song_dict(2),
                              self._song_dict(3)],
                             loads(response.data))

    def test_endpointadapter_make_exception_response(self):
        # Exception message to be expected
        exception_message = 'Unexpected error'
        exception = Exception(exception_message)

        # Prepares the mock
        app_mock = MagicMock()
        endpoint_mock = MagicMock()

        # Instance the endpoint adapter and execute it
        adapter = EndpointAdapter(app_mock, endpoint_mock)
        adapter._make_error_response(exception,
                                     HTTPStatus.INTERNAL_SERVER_ERROR.value)

        app_mock.response_class.assert_called_with(
            status=HTTPStatus.INTERNAL_SERVER_ERROR.value,
            mimetype='application/json',
            response=dumps({'description': exception_message}))

    def test_endpointadapter_songnotfound(self):
        # Exception message to be expected
        exception_message = 'Song X not available'
        exception = SongNotFound(exception_message)

        # Prepares the mock
        app_mock = MagicMock()
        endpoint_mock = MagicMock()
        endpoint_mock.view.side_effect = exception
        request_mock = MagicMock()

        # Instance the endpoint adapter and execute it
        adapter = EndpointAdapter(app_mock, endpoint_mock)
        adapter(request_mock)

        app_mock.response_class.assert_called_with(
            status=HTTPStatus.NOT_FOUND.value,
            mimetype='application/json',
            response=dumps({'description': exception_message}))

    def test_endpointadapter_endpointbadrequest(self):
        # Exception message to be expected
        exception_message = 'Some value error'
        exception = EndpointBadRequest(exception_message)

        # Prepares the mock
        app_mock = MagicMock()
        endpoint_mock = MagicMock()
        endpoint_mock.view.side_effect = exception
        request_mock = MagicMock()

        # Instance the endpoint adapter and execute it
        adapter = EndpointAdapter(app_mock, endpoint_mock)
        adapter(request_mock)

        app_mock.response_class.assert_called_with(
            status=HTTPStatus.BAD_REQUEST.value,
            mimetype='application/json',
            response=dumps({'description': exception_message}))

    def test_endpointadapter_internal_server_error(self):
        # Exception message to be expected
        exception_message = 'Some exception error'
        exception = Exception(exception_message)

        # Prepares the mock
        app_mock = MagicMock()
        endpoint_mock = MagicMock()
        endpoint_mock.view.side_effect = exception
        request_mock = MagicMock()

        # Instance the endpoint adapter and execute it
        adapter = EndpointAdapter(app_mock, endpoint_mock)
        adapter(request_mock)

        app_mock.response_class.assert_called_with(
            status=HTTPStatus.INTERNAL_SERVER_ERROR.value,
            mimetype='application/json',
            response=dumps({'description': exception_message}))
