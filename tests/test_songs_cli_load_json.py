import unittest

from datetime import date
from decimal import Decimal
from json import dumps
from tempfile import NamedTemporaryFile
from unittest.mock import patch

from jsonschema import ValidationError

from songs.cli.load_json import LoadJsonCommand


class TestLoadJsonLoadJsonCommand(unittest.TestCase):
    TITLE = 'Song Title'
    ARTIST = 'Artist Name'
    DIFFICULTY = Decimal(5)
    LEVEL = 5
    RELEASED = date(2012, 12, 12)
    RELEASED_STR = '2012-12-12'

    SONG_DATA = {
        'title': TITLE,
        'artist': ARTIST,
        'difficulty': DIFFICULTY,
        'level': LEVEL,
        'released': RELEASED_STR,
    }

    def setUp(self):
        self.tmpjson = NamedTemporaryFile()
        self.DATA_CONTENTS = [
            {
                'artist': 'The Yousicians',
                'title': 'Lycanthropic Metamorphosis',
                'difficulty': 14.6,
                'level': 13,
                'released': '2016-10-26'
            },
            {
                'artist': 'The Yousicians',
                'title': 'A New Kennel',
                'difficulty': 9.1,
                'level': 9,
                'released': '2010-02-03'
            },
            {
                'artist': 'Mr Fastfinger',
                'title': 'Awaki-Waki',
                'difficulty': 15,
                'level': 13,
                'released': '2012-05-11'
            },
        ]

        self.JSON_CONTENTS = dumps(self.DATA_CONTENTS)

    @patch('songs.apps.songsdb')
    def test_save_song(self, songsdb_mock):

        LoadJsonCommand._save_song(self.SONG_DATA)

        songsdb_mock.make_song.assert_called_with(self.TITLE,
                                                  self.ARTIST,
                                                  self.DIFFICULTY,
                                                  self.LEVEL,
                                                  self.RELEASED_STR)

        songsdb_mock.save_song.assert_called()

    def test_run(self):
        _mock = 'songs.cli.load_json.LoadJsonCommand._save_song'
        with patch(_mock) as save_song_mock:

            LoadJsonCommand._run([self.SONG_DATA])

        save_song_mock.assert_called_once()
        save_song_mock.assert_called_with(self.SONG_DATA)

    def test_command(self):
        LoadJsonCommand._run(self.DATA_CONTENTS)

    def test_validate_songs_json_data(self):
        LoadJsonCommand._validate_songs_json_data(self.DATA_CONTENTS)

    def test_validate_songs_json_data_invalid_data(self):
        with self.assertRaises(ValidationError):
            LoadJsonCommand._validate_songs_json_data({})

    def test_load_songs_json(self):
        self.tmpjson.write(self.JSON_CONTENTS.encode())
        self.tmpjson.flush()

        self.assertEqual(LoadJsonCommand._load_songs_json(self.tmpjson.name),
                         self.DATA_CONTENTS)

    def test_validate_songs_json_arg(self):
        with self.assertRaises(SystemExit) as cm:
            LoadJsonCommand._validate_songs_json_arg('/invalid/path')

        self.assertEqual(cm.exception.code,
                         (1, 'JSON file "/invalid/path" does not exists'))
