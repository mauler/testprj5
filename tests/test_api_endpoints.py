from unittest import TestCase
from unittest.mock import MagicMock

from songs.api import endpoints
from songs.api.endpoint import EndpointBadRequest


class EndpoinTests(TestCase):
    def setUp(self):
        self.app_mock = MagicMock()
        self.db_mock = MagicMock()


class RateSongTests(EndpoinTests):
    endpoint_class = endpoints.RateSong

    SONG_ID = 'abc123'
    RATING = 3

    def test_view(self):
        self.endpoint_class(self.app_mock, self.db_mock).view(
            'POST', {}, {'song_id': self.SONG_ID, 'rating': self.RATING})

        # Preparing mocks
        song_mock = MagicMock()
        self.db_mock._get_song.return_value = song_mock

        # Ensure mocks were called
        self.db_mock._get_song.assert_called_with(self.SONG_ID)

        self.db_mock.rate_song.assert_called()


class ListSongsTests(EndpoinTests):
    endpoint_class = endpoints.ListSongs

    def test_get_page(self):
        # Defaults to page 1
        self.assertEqual(self.endpoint_class._get_page({}), 1)

        # Returns normalized integer
        self.assertEqual(self.endpoint_class
                         ._get_page({self.endpoint_class.PAGE_ARG: '3'}), 3)

        # Error on Invalid Page number
        with self.assertRaises(ValueError) as cm:
            self.endpoint_class._get_page({self.endpoint_class.PAGE_ARG: '-3'})

        self.assertEqual(cm.exception.args[0], 'Invalid page number: -3')

    def test_view(self):

        self.endpoint_class(self.app_mock, self.db_mock).view('GET', {}, {})

        self.db_mock.get_songs.assert_called_with(1)


class SearchSongsTests(EndpoinTests):
    endpoint_class = endpoints.SearchSongs

    SEARCH_MESSAGE = 'the song title'

    def test_get_message_no_message_supplied(self):

        # No message supplied
        with self.assertRaises(EndpointBadRequest) as cm:
            self.endpoint_class._get_message({})

        self.assertEqual(str(cm.exception),
                         'Query argument "message" should ''have a '
                         'valid value')

    def test_get_message_too_short(self):

        # No message supplied
        with self.assertRaises(EndpointBadRequest) as cm:
            self.endpoint_class._get_message({'message': 'some'})

        self.assertEqual(str(cm.exception),
                         'Query argument "message" should ''have a '
                         'valid value')

    def test_get_message(self):

        # No message supplied
        self.assertEqual(self
                         .endpoint_class
                         ._get_message({'message':
                                       self.SEARCH_MESSAGE}),
                         self.SEARCH_MESSAGE)

    def test_view_level_supplied(self):

        self.endpoint_class(self.app_mock, self.db_mock).view(
            'GET',
            {'message': self.SEARCH_MESSAGE},
            {})

        self.db_mock.search_songs.assert_called_with(self.SEARCH_MESSAGE)


class GetSongsAverageDifficultyTests(EndpoinTests):
    endpoint_class = endpoints.GetSongsAverageDifficulty

    LEVEL = 3
    VALID_LEVEL = '3'
    INVALID_LEVEL = '-3'
    INVALID_LEVEL_MESSAGE = 'Invalid level value: -3'

    def test_get_level(self):
        # If no level passed, returns NOne
        self.assertIsNone(self.endpoint_class._get_level({}))

        # Returns normalized integer
        self.assertEqual(self.endpoint_class
                         ._get_level({self.endpoint_class.LEVEL_ARG:
                                     self.VALID_LEVEL}),
                         self.LEVEL)

        # Error on Invalid level
        with self.assertRaises(ValueError) as cm:
            self.endpoint_class._get_level(
                {self.endpoint_class.LEVEL_ARG: self.INVALID_LEVEL})

        self.assertEqual(cm.exception.args[0], self.INVALID_LEVEL_MESSAGE)

    def test_view_no_level_supplied(self):

        self.endpoint_class(self.app_mock, self.db_mock).view('GET', {}, {})

        self.db_mock.get_average_difficulty.assert_called_with()

    def test_view_level_supplied(self):

        self.endpoint_class(self.app_mock, self.db_mock).view(
            'GET',
            {'level': self.LEVEL},
            {})

        self.db_mock.get_average_difficulty_by_level.assert_called_with(
            self.LEVEL)


class GetSongStatisticsTests(EndpoinTests):
    endpoint_class = endpoints.GetSongStatistics

    SONG_ID = 'abcd1234'

    def test_view(self):

        self.endpoint_class(self.app_mock, self.db_mock).view(
            'GET',
            {},
            {},
            self.SONG_ID)

        self.db_mock.get_song_statistics.assert_called_with(self.SONG_ID)
